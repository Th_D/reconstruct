#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>

using namespace cv;
using namespace std;

#define MAX_KEY_POINTS_FULL 10000
#define MAX_KEY_POINTS_FRAG 5000

#define kDistanceCoef 4.0 //todo : essayer d autres val
#define kMaxMatchingSize 50

void detectKeyPtsFull(cv::Mat& img, vector<KeyPoint>& keyPoints, cv::Mat& descriptor)
{
	cv::Ptr<ORB> orb = ORB::create(MAX_KEY_POINTS_FULL);
	orb->detectAndCompute(img, cv::Mat(), keyPoints, descriptor, false);
}

void detectKeyPtsFrag(cv::Mat& img, vector<KeyPoint>& keyPoints, cv::Mat& descriptor)
{
	cv::Ptr<ORB> orb = ORB::create(MAX_KEY_POINTS_FRAG);
	orb->detectAndCompute(img, cv::Mat(), keyPoints, descriptor, false);
}

void showKeyPointsOnImage(cv::Mat &img_color, vector<KeyPoint> &keyPoints)
{
	for(auto &pt : keyPoints)
	{
		//std::cout <<(int)(pt.pt.x+0.5)<<", "<<(int)(pt.pt.y+0.5)<<std::endl;
		img_color.at<Vec3b>(Point((int)(pt.pt.x+0.5),(int)(pt.pt.y+0.5)))[0] = 0;
		img_color.at<Vec3b>(Point((int)(pt.pt.x+0.5),(int)(pt.pt.y+0.5)))[1] = 0;
		img_color.at<Vec3b>(Point((int)(pt.pt.x+0.5),(int)(pt.pt.y+0.5)))[2] = 255;
	}
	cv::imshow("KeyPointsOnImage", img_color);
	cv::waitKey(0);
}

void match(Mat& descriptor1, Mat& descriptor2, vector<DMatch>& matches) {
	matches.clear();

	cv::BFMatcher desc_matcher(cv::NORM_L2, true);
	desc_matcher.match(descriptor1, descriptor2, matches, Mat());

	//?? TODO: bf or knn ?

	std::sort(matches.begin(), matches.end());
	while (matches.front().distance * kDistanceCoef < matches.back().distance) {
		matches.pop_back();
	}
	while (matches.size() > kMaxMatchingSize) {
		matches.pop_back();
	}
}

void ransac(vector<KeyPoint>& keyPoints1, vector<KeyPoint>& keyPoints2,
	vector<DMatch>& matches, vector<char>& match_mask) {
		if (static_cast<int>(match_mask.size()) < 3) {
			return;
		}
		vector<Point2f> pts1;
		vector<Point2f> pts2;
		for (int i = 0; i < static_cast<int>(matches.size()); ++i) {
			pts1.push_back(keyPoints1[matches[i].queryIdx].pt);
			pts2.push_back(keyPoints2[matches[i].trainIdx].pt);
		}
		findHomography(pts1, pts2, cv::RANSAC, 4, match_mask);
	}

	int main(int argc, char** argv){

		if(argc != 3)
		{
			std::cerr<<"Error: invalid use : \""<<argv[0]<<"\""<<std::endl;
			std::cerr<<"Error: try \""<<argv[0]<<" img1 img2\""<<std::endl;
			return -1;
		}

		std::cout<<"Loading "<<argv[1]<<std::endl;
		cv::Mat img1 = cv::imread(argv[1], IMREAD_COLOR );
		cv::Mat img1_color = cv::imread(argv[1], IMREAD_COLOR );

		std::cout<<"Loading "<<argv[2]<<std::endl;
		cv::Mat img2 = cv::imread(argv[2], IMREAD_COLOR );
		cv::Mat img2_color = cv::imread(argv[2], IMREAD_COLOR );

		//On passe les images en gris
		if (img1.channels() != 1) {
			cvtColor(img1, img1, cv::COLOR_RGB2GRAY);
		}
		if (img2.channels() != 1) {
			cvtColor(img2, img2, cv::COLOR_RGB2GRAY);
		}

		// Check loaded images
		if(!img1.data || !img2.data) /// check for invalid input
		{
			std::cerr<<"Error: Could not open or find the image_grey"<<std::endl;
			std::cerr<<"Error: check if ["<<argv[1]<<"] is a correct image file"<<std::endl;
			std::cerr<<"Error: check if ["<<argv[2]<<"] is a correct image file"<<std::endl;
			return -1;
		}

		// Points d'interet
		vector<KeyPoint> keyPoints1;
		vector<KeyPoint> keyPoints2;

		Mat descriptor1;
		Mat descriptor2;

		detectKeyPtsFull(img1, keyPoints1, descriptor1);
		detectKeyPtsFrag(img2, keyPoints2, descriptor2);

		//?? TODO: enlever les points detectes sur les burdures

		unsigned char white[] = {0,0,0};

		showKeyPointsOnImage(img1_color, keyPoints1);

		showKeyPointsOnImage(img2_color, keyPoints2);

		std::cout<<"Orb found "<<keyPoints1.size()<<" key points in "<<argv[1]<<std::endl;
		std::cout<<"Orb found "<<keyPoints2.size()<<" key points"<<argv[2]<<std::endl;

		vector<DMatch> matches;

		match(descriptor1, descriptor2, matches);
		std::cout<<"bf found "<<matches.size()<<" matches"<<std::endl;

		vector<char> match_mask(matches.size(), 1);
		ransac(keyPoints1, keyPoints2, matches, match_mask);
		Mat res;
		cv::drawMatches(img1_color, keyPoints1, img2_color, keyPoints2, matches, res, Scalar::all(-1),
		Scalar::all(-1), match_mask, DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

		cv::imshow("result", res);
		cv::waitKey(0);


		return 0;
	}
